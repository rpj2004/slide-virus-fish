function transforms = gridPhaseCorrelationRegistration(ims,rows,cols,numPeaks, overlapCutoffPercentage)

% Get vars ready and load images
translationMatrix = repmat(struct('left',zeros(1,3),'right',zeros(1,3),'top',zeros(1,3),'bottom',zeros(1,3)),rows,cols);

%se = strel('ball',50,50,8);
%ims = zeros(rows,cols,1024,1024);


% ims is of format {:,:,rows,cols}

% for i = 1:cols
%     for j=1:rows
%         name = ['Scan' '001' '_w1_s' num2str( (i-1) * rows + j) '_t1.TIF'];
% %         ims(j,i,:,:) = imtophat(imread(name),se);
%         ims(j,i,:,:) = imread(name);
%     end
% end




% im1 = squeeze( ims(1,1,:,:) );
% im2 = squeeze( ims(1,2,:,:) );

% Calculate offset based on the user supplied overlapCutoffPercentage,
% which is a loose estimation overlap between images.  Only this region
% of overlap will be passed to the pairwise phase correlation

%nearest even number
offset = 2*round((1024 * overlapCutoffPercentage / 100) / 2);
disp(offset)

% im1 = im1(:,end-offset+1:end);
% im2 = im2(:,1:offset);
% 
% figure
% imshow(im1,[])
% figure
% imshow(im2,[])

%pairwisePhaseCorrelation(im1,im2,numPeaks,overlapCutoffPercentage);

for i = 1:rows
    for j = 1:cols
        if i ~= rows
            translationMatrix( i, j ).bottom = [0 1024-offset 0] + ...
                pairwisePhaseCorrelation( ims(end-offset+1:end,:,i,j), ims(1:offset,:,i+1,j), numPeaks, overlapCutoffPercentage );
        end

        if i~= 1
            translationMatrix( i, j ).top = [0 1024-offset 0] + ...
                pairwisePhaseCorrelation( ims(1:offset,:,i,j), ims(end-offset+1:end,:,i-1,j),  numPeaks, overlapCutoffPercentage );
        
        end
        
        if j ~= cols
            translationMatrix( i, j ).right = [1024-offset 0 0] + ...
                pairwisePhaseCorrelation( ims(:,end-offset+1:end,i,j), ims(:,1:offset,i,j+1), numPeaks, overlapCutoffPercentage );
        end

        if j ~= 1
            translationMatrix( i, j ).left = [1024-offset 0 0] + ...
                pairwisePhaseCorrelation( ims(:,1:offset,i,j), ims(:,end-offset+1:end,i,j-1), numPeaks, overlapCutoffPercentage );
        end
    end
end
% 
xOffset = vertcat(translationMatrix.right,translationMatrix.left);
[~,ind] = sort(xOffset(:,3),'descend');
xOffset = xOffset(ind,:);
yOffset = vertcat(translationMatrix.top,translationMatrix.bottom);
[~,ind] = sort(yOffset(:,3),'descend');
yOffset = yOffset(ind,:);

%    prevMatrix = translationMatrix;
% Crossmatch
    for i = 1:rows-1
        for j = 1:cols-1
            if translationMatrix( i, j ).bottom(3) > translationMatrix( i+1, j ).top(3)
                translationMatrix( i+1, j ).top = translationMatrix( i, j ).bottom;
            else
                translationMatrix( i, j ).bottom = translationMatrix( i+1, j ).top;
            end

            if translationMatrix( i, j ).right(3) > translationMatrix( i, j+1 ).left(3)
                translationMatrix( i, j+1 ).left = translationMatrix( i, j ).right;
            else
                translationMatrix( i, j ).right = translationMatrix( i, j+1 ).left;
            end
        end
    end

xOffset = vertcat(translationMatrix.right);
[~,ind] = sort(xOffset(:,3),'descend');
xOffset = xOffset(ind,:);
yOffset = vertcat(translationMatrix.bottom);
[~,ind] = sort(yOffset(:,3),'descend');
yOffset = yOffset(ind,:);

imageSize = 1024;

% xTranslate = 926;
% yTranslate = 924;
xTranslate = xOffset(1,1);
yTranslate = yOffset(1,2);

fprintf('X Translation is %d, Y Translation is %d\n', xTranslate, yTranslate);

montageXSize = ( imageSize * cols ) - (( cols - 1) * (imageSize - xTranslate) );
montageYSize = ( imageSize * rows ) - (( rows - 1) * (imageSize - yTranslate) );
montage = zeros(montageYSize,montageXSize);
% montage(1:imageSize,1:imageSize) = squeeze(ims(1,1,:,:));

% Generate overlaps
for i = 1:rows
    for j = 1:cols
        
    end
end

% Generate the montage
tempRow = zeros(imageSize,montageXSize);
% For each row, fuse columns
for i = 1:rows
    for j = 1:cols
        tempRow(:, (j-1)*xTranslate + 1 : (j-1)*xTranslate + imageSize ) = ims(:,:,i,j);
    end
    montage( (i-1)*yTranslate + 1 : (i-1)*yTranslate + imageSize, : ) = tempRow;
end

figure
imshow(montage,[]);

transforms = zeros(3,3,rows,cols);

% Generate transforms
for i = 1:rows
    for j=1:cols
        transforms(:,:,i,j) = [1, 0, (j-1)*xTranslate; 0, 1, (i-1)*yTranslate; 0, 0, 1];
    end
end

end