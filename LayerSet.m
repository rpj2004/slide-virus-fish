classdef LayerSet < handle
    properties
        NumLayers = 0;
        ImageSetDimensions;
        Layers;
        AcquisitionDate;
        LayerNames;
        RegionsOfInterest;
    end %properties
    
    %% Public Methods
    methods
        % Constructor
        function ls = LayerSet(layers)
            if nargin > 0
                ls.NumLayers = numel(layers);
                for i = 1:numel(layers)
                    ls.LayerNames{end+1} = layers(i).Name;
                    ls.Layers.(ls.LayerNames{i}) = layers(i);
                end
            end
        end
        
        % Add a layer
        function addLayer(self,layer)
            self.Layers.(layer.LayerName) = montageLayer;
        end
        
        % Montage Layer Set 
        function montageLayerSet(self, channelLayerName, algorithmHandle)
            transforms = algorithmHandle(self.Layers.(channelLayerName));
        end
        
        % Apply transforms to layer set
        function setTransforms(self, transforms)
            for i = 1:self.NumLayers
                self.Layers.(self.LayerNames{i}).setTransforms(transforms);
            end
        end
        
        function getTransforms(self)
        end
    end
    
end %classdef