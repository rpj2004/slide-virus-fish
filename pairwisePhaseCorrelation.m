function translationVectorWithWeight = pairwisePhaseCorrelation(im1, im2, numPeaks, overlapCutoffPercentage)

% Get Image Size
[imageRows,imageCols]=size(im1);

%  figure
% subplot(1,2,1)
% imshow(im1,[])
% subplot(1,2,2)
% imshow(im2,[])

%% Enhance Contrast
rangeOfImages = getrangefromclass(im1);
newMin = rangeOfImages(1);
newMax = rangeOfImages(2);

imMin = min(im1(:));
im1 = (((im1-imMin)*((newMax-newMin)/(max(im1(:))-imMin)))+newMin);

imMin = min(im2(:));
im2 = (((im2-imMin)*((newMax-newMin)/(max(im2(:))-imMin)))+newMin);

gaussianfilter = fspecial('gaussian',[3 3], 0.5);
im1 = imfilter(im1, gaussianfilter, 'replicate');
im2 = imfilter(im2, gaussianfilter, 'replicate');

%  figure
% subplot(1,2,1)
% imshow(im1,[])
% subplot(1,2,2)
% imshow(im2,[])

%% Create and apply window function
S_rate=1000;

% for i=1:length(im1)/2
%       temp(i)             =   exp(-i/(2*S_rate));
% end

for i=1:imageRows / 2
    tempRows(i)             =   1 - ( 1 / ( S_rate ^ (2 * ( 1 - ( i / imageRows ) ) ) ) );
end

for i=1:imageCols / 2
    tempCols(i)             =   1 - ( 1 / ( S_rate ^ (2 * ( 1 - ( i / imageCols ) ) ) ) );
end

tempRows = [fliplr(tempRows) tempRows];
tempCols = [fliplr(tempCols) tempCols];
filtimg1=tempRows(:)*tempCols(:).';

im1fft=double(im1).*filtimg1; % apply windowing function to image
im2fft=double(im2).*filtimg1;

% figure
% imshow(im1fft,[]);
%
% figure
% imshow(im2fft,[]);

%% FFT, normalization to unit vector, multiply by complex conjugate

F1 = fft2(im1fft);
F2 = fft2(im2fft);

F1 = F1 ./ abs(F1);
F2 = F2 ./ abs(F1);
complexConjugateF2 = conj(F2);
normalizedR= F1 .* complexConjugateF2;

result = real(ifft2(normalizedR));

%  figure
%  imshow(result,[])

%% Find Local Maxima and sort by intensity

peaks = result > imdilate(result, [1 1 1; 1 0 1; 1 1 1]);

correlations = repmat(struct('cor',0,'row',0,'col',0,'offset',0),numPeaks*4,1);

a=result .* peaks;

[b,c]=sort(a(:),'descend');

%% Evaluate maxima up to numPeaks times by examining correlation coefficient to weed out bad results

gaussianfilter = fspecial('gaussian',[3 3], 0.5);
im1 = imfilter(im1, gaussianfilter, 'replicate');
im2 = imfilter(im2, gaussianfilter, 'replicate');
% im1 = im1fft;
% im2 = im2fft;


for i=1:numPeaks
    [row,col] = ind2sub(size(a),c(i));
    
    % There are four possibilities given the row and column of the maxima
    for j=1:4
%         if j==1
%             tempImg1 = im1(row:end, col:end);
%             tempImg2 = im2(1:(imageRows-row+1),1:(imageCols-col+1));
%         elseif j==2
%             tempImg1 = im1(1:(imageRows-row+1), col:end);
%             tempImg2 = im2(row:end,1:(imageCols-col+1));
%         elseif j==3
%             tempImg1 = im1(row:end, 1:(imageCols-col+1));
%             tempImg2 = im2(1:(imageRows-row+1), col:end);
%         elseif j==4
%             tempImg1 = im1(1:(imageRows-row+1),1:(imageCols-col+1));
%             tempImg2 = im2(row:end, col:end);
%         end
        if j==1
            tempImg1 = im1(row:end, col:end);
            tempImg2 = im2(1:(imageRows-row+1),1:(imageCols-col+1));
        elseif j==2
            tempImg1 = im1(row:end, 1:col);
            tempImg2 = im2(1:(imageRows-row+1),(imageCols-col+1):end);
        elseif j==3
            tempImg1 = im1(1:row, col:end);
            tempImg2 = im2((imageRows-row+1):end, 1:(imageCols-col+1));
        elseif j==4
            tempImg1 = im1(1:row, 1:col);
            tempImg2 = im2((imageRows-row+1):end, (imageCols-col+1):end);
        end
        if all(size(tempImg1) < 10) || all(size(tempImg2) < 10) || numel(tempImg1) < numel(im1) * ((overlapCutoffPercentage/100) ^ 2)
            correlations(4*(i-1)+j).cor = 0;
        else
            %temp = tempImg1 ./ tempImg2;
            %temp = temp / mean(temp(:));
            %correlations(4*(i-1)+j).cor = std(double(temp(:)));
            correlations(4*(i-1)+j).cor = corr2(tempImg1(:,1:end-1),tempImg2(:,2:end));
        end
        correlations(4*(i-1)+j).row = row;
        correlations(4*(i-1)+j).col = col;
        correlations(4*(i-1)+j).offset = j;
    end
    
end

%% Return as translation vector
[d,e] = sort([correlations.cor],'descend');

row = correlations(e(1)).row;
col = correlations(e(1)).col;
t3 = correlations(e(1)).offset;

% fprintf('%d offset used\n',t3);

if t3==1
    ty = row-1;
    tx = col-1;
elseif t3==2
    ty = row-1;
    tx = imageCols - col;
elseif t3==3
    ty = imageRows - row;
    tx = col-1;
elseif t3==4
    ty = imageRows - row;
    tx = imageCols - col;
end
fprintf('DeltaX %d, DeltaY %d, Correlation Coeff %f, Offset Used %d\n',tx,ty,correlations(e(1)).cor,t3);
translationVectorWithWeight = [tx ty correlations(e(1)).cor];
% if ty == 168
% disp(ty);
% end
end