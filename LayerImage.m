classdef LayerImage < handle
    properties
        TransformMatrix;
        ImageDimensions;
        ImageName;
        Image;
        GridRow;
        GridCol;
    end %properties
    
    
    properties (Dependent = true, SetAccess = protected)
        GlobalLocation;
    end
    %% Public Methods
    methods
        % Constructor
        function li = LayerImage(image, gridRow, gridCol, varargin)
            if nargin > 0
                li.Image = image;
                li.ImageDimensions = size(image);
                li.GridRow = gridRow;
                li.GridCol = gridCol;
                
                if nargin == 3
                    li.ImageName = 'Default';
                    li.TransformMatrix = eye(3,3);
                elseif nargin == 4
                    li.ImageName = varargin{1};
                    li.TransformMatrix = eye(3,3);

                elseif nargin > 4
                    li.ImageName = varargin{1};
                    li.TransformMatrix = varargin{2};
                end
            end
        end
        
        % Set functions
        function set.TransformMatrix(self,transformMatrix)
            if ~all(size(transformMatrix) == [3,3])
                error('Transformation matrix must be 3x3');
            end
            
            self.TransformMatrix = transformMatrix;
        end
        
        function set.Image(self,image)
            if ~isa(image, 'uint16')
                error('Image must be in uint16 format');
            end
            
            self.Image = image;
        end
        
        function set.ImageName(self,imageName)
            if ~ischar(imageName)
                error('Image name must be a string');
            end
            
            self.ImageName = imageName;
        end
        
        % Get functions
        
        function globalLocation = get.GlobalLocation(self)
            globalLocation = [self.TransformMatrix(1,3) self.TransformMatrix(2,3)];
        end
        % Get value of pixel at location
        function getPixel(self,x,y)
            self.Image(x,y)
        end
        
        
        function imshow(self)
            imshow(self.Image,[]);
        end
        
        % Return the x by y uint16 image array from the LayerImage object
        function out = getImageAsArray(self)
            out = cat(3,(self.Image));
        end
        
    end % Public Methods
    
    %% Private Methods
    methods (Access = private)
        
    end % Private Methods
    
end %classdef