classdef MontageAlgorithm < handle
    % This class provides a common interface to apply montaging
    % functions to LayerSet objects
    properties
        AlgorithmHandle;
        Parameters;
    end % properties
    
    methods
        % constructor
        function ma = MontageAlgorithm(parameters, algorithmHandle)
            if nargin > 0
                ma.Parameters = parameters;
                ma.AlgorithmHandle = algorithmHandle;
            end
        end
        
        % apply montaging function to layerset or channel objects
        function Align(self, channelLayerOrLayerSet, varargin)
            if isa(channelLayerOrLayerSet,'ChannelLayer')
                transforms = doAlignChannel(self, channelLayerOrLayerSet);
                channelLayerOrLayerSet.setTransforms(transforms);
            elseif isa(channelLayerOrLayerSet,'LayerSet')
                if nargin ~= 3
                    error('If LayerSet is supplied, the reference channel name must be supplied as the second argument');
                elseif nargin == 3
                    transforms = doAlignChannel(self, channelLayerOrLayerSet.Layers.(varargin{1}));
                    channelLayerOrLayerSet.setTransforms(transforms);
                end
            end
        end
        
    end % methods
    
    methods (Access=private)
        function transforms = doAlignChannel(self, channelLayer)
            transforms = self.AlgorithmHandle(channelLayer.getImagesAsArray(), channelLayer.Rows, channelLayer.Cols, self.Parameters{:});
        end
    end
    
end % classdef