classdef ChannelLayer < handle
    properties
        LayerImages = LayerImage.empty();
        NumImages = 0;
        Rows = 0;
        Cols = 0;
        Pseudocolor;
        Name;
        ExposureTime;
        Fluorophore;
    end %properties

    properties (Dependent = true, SetAccess = protected)
        Dimensions;
        Montage;
    end
    
    %% Public Methods
    methods 
        % Constructor
        function ml = ChannelLayer(layerImages,name)
            if nargin > 0
                ml.LayerImages = layerImages;
                [ml.Rows, ml.Cols] = size(layerImages);
                ml.NumImages = numel(layerImages);
                ml.Name = name;
            end
        end
        
        % Get Methods
        function dimensions = get.Dimensions(self)
            if ~isempty(self.LayerImages)
                dimensions = (self.LayerImages(self.Rows,self.Cols).GlobalLocation + self.LayerImages(self.Rows,self.Cols).ImageDimensions)...
                    - self.LayerImages(1,1).GlobalLocation;
                dimensions = fliplr(dimensions);
            else
                error('Layer does not contain any images');
            end
        end
        
        function montage = get.Montage(self)
            if ~isempty(self.LayerImages)
                montage = zeros(self.Dimensions,'uint16');
                for i = 1:self.Rows
                    for j = 1:self.Cols
                        xTranslation = self.LayerImages(i,j).GlobalLocation(1);
                        imageSizeX = self.LayerImages(i,j).ImageDimensions(1);
                        image = self.LayerImages(i,j).Image;
                        tempRow(:, xTranslation + 1 : xTranslation + imageSizeX ) = image;
                    end
                    yTranslation = self.LayerImages(i,j).GlobalLocation(2);
                    imageSizeY = self.LayerImages(i,j).ImageDimensions(2);
                    montage( yTranslation + 1 : yTranslation + imageSizeY, : ) = tempRow;
                end
            else
                error('Layer does not contain any images');
            end
        end
        
        % Set Methods
        function set.LayerImages(self,layerImages)
            if ~isa(layerImages,'LayerImage')
                error('LayerImages must be an array of LayerImage class objects');
            end
            self.LayerImages = layerImages;
        end
        
        function set.Dimensions(~,~)
            error('Dimensions of the layer cannot be set explicitly. It is calculated from images in the layer.');
        end
        
        function set.Montage(~,~)
            error('Montage cannot be set explicitly. It is calculated from images in the layer.');
        end
        
        
        % Add Images to Layer
        function addImage(self,layerImage)
            self.LayerImages(end+1) = layerImage;
        end
        
        % Get images from layer as an array
        function out = getImagesAsArray(self)
            out = reshape(self.LayerImages.getImageAsArray(),horzcat(self.LayerImages(1).ImageDimensions,self.Rows,self.Cols));
        end
        
        % Apply transforms to the layer
        function setTransforms(self, transforms)
            nElem = numel(self.LayerImages);
            transforms = transforms(:,:,:);
            if nElem ~= size( transforms, 3 )
                error('Number of transforms must equal the number of images in the layer');
            else
                for i = 1:nElem
                    self.LayerImages(i).TransformMatrix = transforms(:,:,i);
                end
            end
        end
        
        % Get transforms
        function getTransforms(self)
        end
        
        function imshow(self)
            imshow(self.Montage,[]);
        end
    end %methods
    
    %% Private Methods
    methods (Access = private) 
        
    end % Private Methods
    
    
end %classdef